package info.dhickcunk.tripmajalengka;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import info.dhickcunk.tripmajalengka.adapter.TempatListAdapter;
import info.dhickcunk.tripmajalengka.adapter.ViewPagerAdapter;
import info.dhickcunk.tripmajalengka.model.Tempat;
import info.dhickcunk.tripmajalengka.service.APIService;
import me.relex.circleindicator.CircleIndicator;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class DetailActivity extends AppCompatActivity {
    private ViewPager viewPager;
    CircleIndicator indicator;
    TextView textNamaTempat, textNamaKategori, textHargaTiket, textDeskripsi, textAlamat;
    SwipeRefreshLayout mSwipeRefreshLayout;
    String alamat = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewPager = (ViewPager)findViewById(R.id.viewPager);
        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.kontenRefreshView);
        textNamaTempat = (TextView)findViewById(R.id.textNamaTempat);
        textNamaKategori = (TextView)findViewById(R.id.textNamaKategori);
        textHargaTiket = (TextView)findViewById(R.id.textHargaTiket);
        textDeskripsi = (TextView)findViewById(R.id.textDeskripsi);
        textAlamat = (TextView)findViewById(R.id.textAlamat);
        getSupportActionBar().setTitle("Detail Tempat");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mSwipeRefreshLayout.setProgressViewOffset(false, 0, 110);
        }

        Intent intent = getIntent();
        final int idTempat = Integer.parseInt(intent.getExtras().getString("idTempat"));

        getTempatByKategori(idTempat);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getTempatByKategori(idTempat);
            }
        });
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 2000, 4000);
    }

    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            DetailActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() == 0){
                        viewPager.setCurrentItem(1);
                    }else if(viewPager.getCurrentItem() == 1){
                        viewPager.setCurrentItem(2);
                    }else if(viewPager.getCurrentItem() == 2) {
                        viewPager.setCurrentItem(3);
                    }else{
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }

    private void getTempatByKategori(int idTempat){
        mSwipeRefreshLayout.setRefreshing(true);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://dhickcunk.info/trip_majalengka/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<Tempat> call = service.getTempatById(idTempat);

        call.enqueue(new Callback<Tempat>() {
            @Override
            public void onResponse(Response<Tempat> response, Retrofit retrofit) {
                Tempat tempat = response.body();

                if (!tempat.getData().isEmpty()){
                    ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(DetailActivity.this, tempat.getData().get(0).getFoto());
                    viewPager.setAdapter(viewPagerAdapter);
                    indicator.setViewPager(viewPager);
                    textNamaTempat.setText(tempat.getData().get(0).getNamaTempat());
                    textNamaKategori.setText(tempat.getData().get(0).getNamaKategori());
                    textHargaTiket.setText("Rp. "+tempat.getData().get(0).getHargaTiket());
                    textDeskripsi.setText(tempat.getData().get(0).getDeskripsi());
                    textAlamat.setText(tempat.getData().get(0).getAlamat());
                    alamat = tempat.getData().get(0).getAlamat();
                    mSwipeRefreshLayout.setRefreshing(false);
                }else {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Gagal",t.getMessage(), t);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.map:

                String map = "http://maps.google.co.in/maps?q=" + textAlamat.getText().toString().trim();
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                startActivity(i);

                Toast.makeText(DetailActivity.this, alamat, Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detail, menu);

        return super.onCreateOptionsMenu(menu);
    }
    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }
}
