package info.dhickcunk.tripmajalengka;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import uk.co.senab.photoview.PhotoViewAttacher;

public class ZoomGambarActivity extends AppCompatActivity {
    private ImageView image;
    public static String KEY_IMAGE = "image";
    private String imageUrl;
    private PhotoViewAttacher photoViewAttacher = null;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_gambar);
        getSupportActionBar().hide();

        image = (ImageView)findViewById(R.id.image);
        progressBar = (ProgressBar)findViewById(R.id.progress);

        imageUrl = getIntent().getStringExtra(KEY_IMAGE);
        photoViewAttacher = new PhotoViewAttacher(image);
        Glide.with(this)
                .load("http://dhickcunk.info/trip_majalengka/images/"+ imageUrl)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(image);
    }

    public static void toDetailImageActivity(Activity activity, String imageUrl){
        Intent intent = new Intent(activity, ZoomGambarActivity.class);
        intent.putExtra(KEY_IMAGE, imageUrl);
        activity.startActivityForResult(intent, 0);
    }
}
