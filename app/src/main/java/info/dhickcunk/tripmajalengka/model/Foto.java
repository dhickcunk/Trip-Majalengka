package info.dhickcunk.tripmajalengka.model;

/**
 * Created by Dicky Gusthia S on 17/05/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Foto {

    @SerializedName("id_foto")
    @Expose
    private String idFoto;
    @SerializedName("url")
    @Expose
    private String url;

    public String getIdFoto() {
        return idFoto;
    }

    public void setIdFoto(String idFoto) {
        this.idFoto = idFoto;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}