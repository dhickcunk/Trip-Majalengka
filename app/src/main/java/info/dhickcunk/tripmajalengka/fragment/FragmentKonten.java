package info.dhickcunk.tripmajalengka.fragment;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import info.dhickcunk.tripmajalengka.MainActivity;
import info.dhickcunk.tripmajalengka.R;
import info.dhickcunk.tripmajalengka.adapter.AdapterNavItem;
import info.dhickcunk.tripmajalengka.adapter.TempatListAdapter;
import info.dhickcunk.tripmajalengka.model.Data;
import info.dhickcunk.tripmajalengka.model.Datum;
import info.dhickcunk.tripmajalengka.model.Foto;
import info.dhickcunk.tripmajalengka.model.Kategori;
import info.dhickcunk.tripmajalengka.model.Tempat;
import info.dhickcunk.tripmajalengka.service.APIService;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentKonten extends Fragment {
    public static Context activity ;
    RecyclerView listTempatRec;
    LinearLayoutManager linearLayoutManager;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView textKosong;

    public FragmentKonten newInstance(Context activity) {
        this.activity = activity ;
        return new FragmentKonten();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_konten, container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.kontenRefreshView);
        listTempatRec = (RecyclerView)view.findViewById(R.id.list);
        textKosong = (TextView)view.findViewById(R.id.textKosong);

        linearLayoutManager = new LinearLayoutManager(activity);
        listTempatRec.setLayoutManager(linearLayoutManager);

        final int getArgument = Integer.parseInt(getArguments().getString("data"));
        if (getArgument == 1){
            getAllTempat();
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getAllTempat();
                }
            });
        }else{
            getTempatByKategori(getArgument);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getTempatByKategori(getArgument);
                }
            });
        }
        return view;
    }

    private void getAllTempat(){
        mSwipeRefreshLayout.setRefreshing(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://dhickcunk.info/trip_majalengka/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<Tempat> call = service.getAllTempat();

        call.enqueue(new Callback<Tempat>() {
            @Override
            public void onResponse(Response<Tempat> response, Retrofit retrofit) {
                Tempat tempat = response.body();

                if (!tempat.getData().isEmpty()){
                    listTempatRec.setAdapter(new TempatListAdapter(tempat, activity));
                }else {
                    textKosong.setVisibility(View.VISIBLE);
                    listTempatRec.setVisibility(View.GONE);
                }
                mSwipeRefreshLayout.setRefreshing(false);
                getActivity().setTitle(tempat.getData().get(0).getNamaKategori());
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Gagal",t.getMessage(), t);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    private void getTempatByKategori(int idKategori){
        mSwipeRefreshLayout.setRefreshing(true);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://dhickcunk.info/trip_majalengka/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<Tempat> call = service.getTempatByKategori(idKategori);

        call.enqueue(new Callback<Tempat>() {
            @Override
            public void onResponse(Response<Tempat> response, Retrofit retrofit) {
                Tempat tempat = response.body();

                if (!tempat.getData().isEmpty()){
                    listTempatRec.setAdapter(new TempatListAdapter(tempat, activity));
                }else {
                    textKosong.setVisibility(View.VISIBLE);
                    listTempatRec.setVisibility(View.GONE);
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Gagal",t.getMessage(), t);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }
}
