package info.dhickcunk.tripmajalengka.service;

import info.dhickcunk.tripmajalengka.model.Kategori;
import info.dhickcunk.tripmajalengka.model.Tempat;
import okhttp3.ResponseBody;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;

public interface APIService {

    @GET("tempat.php?action=get_all")
    Call<Tempat> getAllTempat();

    @GET("tempat.php?action=get_by_id")
    Call<Tempat> getTempatById(@Query("tempat") int tempat);

    @GET("tempat.php?action=get_by_kategori")
    Call<Tempat> getTempatByKategori(@Query("kategori") int kategori);

    @GET("kategori.php")
    Call<Kategori> getKategori();
}
