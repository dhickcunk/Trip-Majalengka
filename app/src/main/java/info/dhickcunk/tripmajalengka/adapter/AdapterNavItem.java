package info.dhickcunk.tripmajalengka.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

import info.dhickcunk.tripmajalengka.R;
import info.dhickcunk.tripmajalengka.model.Kategori;

/**
 * Created by Dicky Gusthia S on 18/05/2017.
 */

public class AdapterNavItem extends BaseAdapter {
    ArrayList<HashMap<String, String>> arr;
    Kategori kategori;
    Activity activity;

    public AdapterNavItem(Activity activity, Kategori kategori){
        this.activity = activity;
        this.kategori = kategori;
    }

    @Override
    public int getCount() {
        return kategori.getData().size();
    }

    @Override
    public Object getItem(int position) {
        return kategori.getData();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder = null;

        if (view == null){
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_nav_drawer, null);
            holder.iconKategori = (ImageView) view.findViewById(R.id.kategoriIcon);
            holder.namaKategori = (TextView)view.findViewById(R.id.kategoriName);
            view.setTag(holder);
        }else{
            holder = (ViewHolder)view.getTag();
        }
        String icon = kategori.getData().get(position).getIcon();
        String nama = kategori.getData().get(position).getNamaKategori();
        Glide.with(activity)
                .load("http://dhickcunk.info/trip_majalengka/images/"+ icon)
                .placeholder(R.mipmap.ic_launcher_round)
                .into(holder.iconKategori);
        holder.namaKategori.setText(nama);

        return view;
    }

    static class ViewHolder{
        ImageView iconKategori;
        TextView namaKategori;
    }
}
