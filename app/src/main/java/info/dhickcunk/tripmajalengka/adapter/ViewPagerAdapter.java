package info.dhickcunk.tripmajalengka.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import info.dhickcunk.tripmajalengka.R;
import info.dhickcunk.tripmajalengka.ZoomGambarActivity;
import info.dhickcunk.tripmajalengka.model.Foto;

import static info.dhickcunk.tripmajalengka.ZoomGambarActivity.KEY_IMAGE;

/**
 * Created by Dicky Gusthia S on 17/05/2017.
 */

public class ViewPagerAdapter extends PagerAdapter {
    Context activity;
    LayoutInflater layoutInflater;
    List<Foto> fotoList;
    private ImageView imageSlider;
    private ProgressBar progressBar;

    public ViewPagerAdapter(Activity activity, List<Foto> fotoList) {
        this.fotoList = fotoList;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return fotoList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_image_slider, null);

        imageSlider = (ImageView) view.findViewById(R.id.image_slider);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);

        Glide.with(activity)
                .load("http://dhickcunk.info/trip_majalengka/images/"+ fotoList.get(position).getUrl())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imageSlider);
        ViewPager vp = (ViewPager)container;
        vp.addView(view, 0);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ZoomGambarActivity.class);
                intent.putExtra(KEY_IMAGE, fotoList.get(position).getUrl());
                activity.startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager vp = (ViewPager) container;

        View view = (View) object;
        vp.removeView(view);
    }
}
