package info.dhickcunk.tripmajalengka.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.HashMap;

import info.dhickcunk.tripmajalengka.DetailActivity;
import info.dhickcunk.tripmajalengka.R;
import info.dhickcunk.tripmajalengka.model.Tempat;

public class TempatListAdapter extends RecyclerView.Adapter<TempatListAdapter.ViewHolder> {

    Tempat tempat;
    private Context activity;
    View viewRoot;

    public TempatListAdapter(Tempat tempat, Context activity) {
        this.tempat = tempat;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        String namaTempat = tempat.getData().get(position).getNamaTempat();
        String deskripsi = tempat.getData().get(position).getDeskripsi();
        String kategori = tempat.getData().get(position).getNamaKategori();
        String foto = tempat.getData().get(position).getFoto().get(0).getUrl();

        Glide.with(activity)
                .load("http://dhickcunk.info/trip_majalengka/images/"+foto)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        viewHolder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        viewHolder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(viewHolder.fotoTempat);
        viewHolder.textNamaTempat.setText(namaTempat);
        viewHolder.textDeskripsi.setText(deskripsi);
        viewHolder.textKategori.setText(kategori);
    }

    @Override
    public int getItemCount() {
        return tempat.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView fotoTempat;
        TextView textNamaTempat;
        TextView textDeskripsi;
        TextView textKategori;
        ProgressBar progressBar;

        public ViewHolder(View view) {
            super(view);
            fotoTempat = (ImageView) view.findViewById(R.id.fotoTempat);
            textNamaTempat = (TextView) view.findViewById(R.id.textNamaTempat);
            textKategori = (TextView) view.findViewById(R.id.textKategori);
            textDeskripsi = (TextView) view.findViewById(R.id.textDeskripsi);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String idTempat = tempat.getData().get(getAdapterPosition()).getIdTempat();
            Intent intent = new Intent(activity, DetailActivity.class);
            intent.putExtra("idTempat", idTempat);
            activity.startActivity(intent);
        }
    }
}
